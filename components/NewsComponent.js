// src/components/NewsComponent.js
import React from 'react';
import { View, Text, ActivityIndicator, Linking, ScrollView, StyleSheet, Image } from 'react-native';
import { useInfiniteQuery, QueryClient, QueryClientProvider } from 'react-query';

const fetchNews = async ({ pageParam = 1 }) => {
  const response = await fetch(`https://saurav.tech/NewsAPI/everything/cnn.json?page=${pageParam}`);
  if (!response.ok) {
    throw new Error('Network response was not ok');
  }
  return response.json();
};

const NewsComponent = () => {
  const {
    data,
    isLoading,
    isError,
    error,
    fetchNextPage,
    hasNextPage,
    isFetchingNextPage,
  } = useInfiniteQuery('news', fetchNews, {
    getNextPageParam: (lastPage, allPages) => {
      return lastPage.page + 1; // Assuming the API returns a page parameter
    },
  });

  if (isLoading) {
    return (
      <View style={styles.container}>
        <ActivityIndicator size="large" color="#0000ff" />
      </View>
    );
  }

  if (isError) {
    return (
      <View style={styles.container}>
        <Text>Error: {error.message}</Text>
      </View>
    );
  }

  return (
    <ScrollView
      contentContainerStyle={styles.scrollViewContainer}
      onScroll={({ nativeEvent }) => {
        if (
          nativeEvent.layoutMeasurement.height + nativeEvent.contentOffset.y >=
          nativeEvent.contentSize.height - 20
        ) {
          if (hasNextPage && !isFetchingNextPage) {
            fetchNextPage();
          }
        }
      }}
      scrollEventThrottle={400}
    >
      <Text style={styles.title}> News</Text>
      {data.pages.map((page, pageIndex) => (
        <React.Fragment key={pageIndex}>
          {page.articles.map((article, index) => (
            <View key={index} style={styles.cardContainer}>
              {article.urlToImage && (
                <Image source={{ uri: article.urlToImage }} style={styles.image} />
              )}
              <View style={styles.textContainer}>
                <Text style={styles.articleTitle}>{article.title}</Text>
                <Text style={styles.articleDescription}>{article.description}</Text>
                <Text onPress={() => Linking.openURL(article.url)} style={styles.readMore}>Read more</Text>
              </View>
            </View>
          ))}
        </React.Fragment>
      ))}
      {isFetchingNextPage && (
        <View style={styles.loadingIndicator}>
          <ActivityIndicator size="small" color="#0000ff" />
        </View>
      )}
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    
  },
  scrollViewContainer: {
    paddingHorizontal: 16,
    paddingVertical: 20,
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    textAlign: 'center',
    marginBottom: 20,
    marginTop:30,
  },
  cardContainer: {
    backgroundColor: '#fff',
    borderRadius: 8,
    borderWidth: 1,
    borderColor: '#ddd',
    marginBottom: 20,
    flexDirection: 'row',
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.2,
    shadowRadius: 2,
    elevation: 3,
  },
  textContainer: {
    flex: 1,
    padding: 8,
  },
  image: {
    marginTop:2,
    borderTopLeftRadius: 8,
    borderBottomLeftRadius: 8,
    width: 150,
    height: 180,
    resizeMode: 'cover',
  },
  articleTitle: {
    fontSize: 17,
    fontWeight: 'bold',
    marginBottom: 5,
  },
  articleDescription: {
    fontSize: 13,
    marginBottom: 10,
  },
  readMore: {
    color: 'blue',
  },
  loadingIndicator: {
    alignItems: 'center',
    marginVertical: 20,
  },
});

export default NewsComponent;
