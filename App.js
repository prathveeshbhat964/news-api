import React from 'react';
import { View } from 'react-native';
import NewsComponent from './components/NewsComponent';
import { QueryClient, QueryClientProvider } from 'react-query';

const queryClient = new QueryClient();

const App = () => {
  return (
    <QueryClientProvider client={queryClient}>
      <View style={{ flex: 1 }}>
        <NewsComponent />
      </View>
    </QueryClientProvider>
  );
};

export default App;
